@echo off
:: variables
set DOCKER_USERNAME=jmutsers
set DOCKER_PASSWORD=%2
set CONTAINER=%1
set IMAGE=%DOCKER_USERNAME%/%CONTAINER%
set PORTS=8761:8761

echo %IMAGE%

git describe --always --abbrev --tags --long > result.txt
set /p GIT_VERSION=<result.txt

:: Build and set tag for image
docker build -t "%IMAGE%:%GIT_VERSION%" .
docker tag %IMAGE%:%GIT_VERSION% %IMAGE%:latest

:: Log in to Docker Hub and push
docker login -u %DOCKER_USERNAME% -p %DOCKER_PASSWORD%
docker push %IMAGE%:%GIT_VERSION%
docker push %IMAGE%:latest
