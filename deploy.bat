@echo off

set CONTAINER=kwetter-eureka-server
set /p DOCKER_PASSWORD=<docker_password.txt

:: use CALL when running another script file from a script file
CALL mvn clean install -DskipTests
CALL ./build_image.bat %CONTAINER% %DOCKER_PASSWORD%

del result.txt