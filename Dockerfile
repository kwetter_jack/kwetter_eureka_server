#
# Build stage
#
FROM maven:3.8.5-openjdk-17 AS build
COPY src /home/app/src
COPY pom.xml /home/app
RUN mvn -f /home/app/pom.xml clean install -DskipTests

FROM openjdk:11
EXPOSE 8761
COPY --from=build /home/app/target/eureka_server-0.0.1-SNAPSHOT.jar eureka_server.jar
CMD ["java","-jar","eureka_server.jar"]
